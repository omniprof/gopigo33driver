package com.kenfogel.gopigo3driver;

import com.pi4j.io.spi.SpiChannel;
import com.pi4j.io.spi.SpiDevice;
import com.pi4j.io.spi.SpiFactory;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.kenfogel.gopigo3driver.constants.SPI_MESSAGE_TYPE.*;

import java.util.Arrays;

/**
 * Created by jabrena on 20/7/17. Update by Ken Fogel 2018/10/04
 *
 * https://www.dexterindustries.com/GoPiGo3/
 * https://github.com/DexterInd/GoPiGo3
 *
 * Portions Copyright (c) 2017 Dexter Industries Released under the MIT license
 * (http://choosealicense.com/licenses/mit/). For more information see
 * https://github.com/DexterInd/GoPiGo3/blob/master/LICENSE.md
 *
 */
public class GoPiGo3 {

    static final Logger LOG = Logger.getLogger(GoPiGo3.class.getName());
    // SPI out array

    private final byte[] spi_array_out;
    private byte[] spi_array_in = null;  // SPI in array

    // SPI device
    private SpiDevice spi = null;

    public GoPiGo3() throws IOException {
        this.spi_array_out = new byte[LONGEST_SPI_TRANSFER];

        // create SPI object instance for SPI for communication
        spi = SpiFactory.getInstance(SpiChannel.CS1); // default spi mode 0
    }

    /**
     * Builder for output arrays
     *
     * @param args
     */
    public void spiArrayBuilder(byte... args) {
        Arrays.fill(spi_array_out, (byte) 0);

        for (int x = 0; x < args.length; ++x) {
            spi_array_out[x] = args[x];
        }
    }

    /**
     * Run a specific motor
     *
     * @param motor
     * @param power
     * @throws IOException
     */
    public void runMotor(byte motor, byte power) throws IOException {
        spiArrayBuilder(ADDRESS, SET_MOTOR_PWM, motor, power);
        spi.write(spi_array_out);
    }

    /**
     * Activate a specific servo (1 or 2) in a sweeping motion
     *
     * @param servo
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public void set_servo(byte servo) throws IOException, InterruptedException {
        spiArrayBuilder(ADDRESS, GPGSPI_MESSAGE_SET_SERVO, servo);
        while (true) {
            for (int i = 1000; i < 2001; ++i) {// count from 1000 to 2000
                spi_array_out[3] = (byte) (i >>> 8);
                spi_array_out[4] = (byte) (i);
                spi.write(spi_array_out);
                Thread.sleep(2);
            }
            for (int i = 1000; i < 2001; ++i) {// count from 1000 to 2000
                spi_array_out[3] = (byte) (((3000 - i) >>> 8));
                spi_array_out[4] = (byte) ((3000 - i));
                spi.write(spi_array_out);
                Thread.sleep(2);
            }
        }
    }

    /**
     * Stop the servo
     *
     * @throws IOException
     */
    public void stop_servo() throws IOException {
        LOG.log(Level.INFO, "stop_servo");
        spiArrayBuilder(ADDRESS, GPGSPI_MESSAGE_SET_SERVO, (byte) 0x01, (byte) (0), (byte) (0));
        spi.write(spi_array_out);
    }

    /**
     * Run a specific motor
     *
     * @param power
     * @throws IOException
     */
    public void runBothMotors(byte power) throws IOException {
        spiArrayBuilder(ADDRESS, SET_MOTOR_PWM, (byte) 0x01, power);
        spi.write(spi_array_out);

        spi_array_out[2] = 2; // 1 | 2
        spi.write(spi_array_out);
    }

    /**
     * Stop a specific motor
     *
     * @param motor
     * @throws IOException
     */
    public void stopMotor(byte motor) throws IOException {
        this.runMotor(motor, (byte) 0);
    }

    /**
     * Stop all motors
     *
     * @throws IOException
     */
    public void stopMotors() throws IOException {
        this.runMotor(MOTOR_LEFT, (byte) 0);
        this.runMotor(MOTOR_RIGHT, (byte) 0);
    }

    /**
     * Light up left eye LED.
     *
     * @param red
     * @param green
     * @param blue
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public byte[] doLeftEyeLED(final byte red, final byte green, final byte blue) throws IOException, InterruptedException {
        Arrays.fill(spi_array_out, (byte) 0);
        spiArrayBuilder(ADDRESS, SET_LED, LED_EYE_LEFT, red, green, blue);
        return spi.write(spi_array_out);
    }

    /**
     * Light up right eye LED.
     *
     * @param red
     * @param green
     * @param blue
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public byte[] doRightEyeLED(final byte red, final byte green, final byte blue) throws IOException, InterruptedException {
        Arrays.fill(spi_array_out, (byte) 0);
        spiArrayBuilder(ADDRESS, SET_LED, LED_EYE_RIGHT, red, green, blue);
        return spi.write(spi_array_out);
    }

    /**
     * Retrieve UltrasonicSensor readings
     *
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public int doUltra() throws IOException, InterruptedException {
        set_ultrasonic_grove_type();
        return get_ultrasonic_grove_value();
    }

    /**
     * Send the message to the board that you intend to read from the Ultrasonic
     * Sensor
     *
     * @return spi_array_in if needed for debugging
     */
    private byte[] set_ultrasonic_grove_type() {
        Arrays.fill(spi_array_out, (byte) 0);
        spiArrayBuilder(ADDRESS, GPGSPI_MESSAGE_SET_GROVE_TYPE, GROVE_1, GROVE_TYPE_US);

        try {
            spi_array_in = spi.write(spi_array_out);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Error writing to spi.", ex);
        }

        try {
            Thread.sleep(250);
        } catch (InterruptedException ex) {
            LOG.log(Level.SEVERE, "Error writing to spi.", ex);
        }

        return spi_array_in;
    }

    /**
     * Read from the Ultrasonic sensor
     *
     * @return spi_array_in if needed for debugging
     */
    private int get_ultrasonic_grove_value() {
        int spi_transfer_length = 8;
        spiArrayBuilder(ADDRESS, GPGSPI_MESSAGE_GET_GROVE_VALUE_1);

        try {
            spi_array_in = spi.write(spi_array_out, 0, spi_transfer_length);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Error writing to spi.", ex);
        }
        try {
            Thread.sleep(250);
        } catch (InterruptedException ex) {
            LOG.log(Level.SEVERE, "Interupted Thread.sleep", ex);
        }

        //If the fourth byte received is not 0xA5
        //  if(spi_array_in[4] != 0xA5){
        //    return null;
        //  }
        return (((spi_array_in[6] << 8) & 0xFF00) | (spi_array_in[7] & 0xFF));
    }

    /**
     * The following section is a work in progress. The manufacturer and board
     * names work but there are still problems with the numeric data
     */
    
    /**
     * Retrieve the name of the manufacturer of the GoPiGo3. Should be Dexter
     * Industries
     *
     * @return
     */
    String get_manufacturer() {
        return spi_read_string(GPGSPI_MESSAGE_GET_MANUFACTURER);
    }

    /**
     * Retrieve the name of the GoPiGo3 board
     *
     * @return
     */
    public String get_board() {
        return spi_read_string(GPGSPI_MESSAGE_GET_NAME);
    }

    /**
     * Retrieve the hardware version 32 bit
     *
     * @return
     */
    public String get_version_hardware() {
        int data;
        // assign error to the value returned by spi_read_32, and if not 0:
        data = spi_read(GPGSPI_MESSAGE_GET_HARDWARE_VERSION);

        return String.format("%d.x.x", data);
    }

    /**
     * Retrieve the firmware version 32 bit
     *
     * @return
     */
    public String get_version_firmware() {
        int data;
        // assign error to the value returned by spi_read_32, and if not 0:
        data = spi_read(GPGSPI_MESSAGE_GET_FIRMWARE_VERSION);

        return String.format("%d.%d.%d", (data / 1000000), ((data / 1000) % 1000), (data % 1000));

    }

    /**
     * Get the ID number
     *
     * @return
     */
    public String get_id() {

        Arrays.fill(spi_array_out, (byte) 0);
        StringBuilder response = new StringBuilder();
        spiArrayBuilder(ADDRESS, GPGSPI_MESSAGE_GET_ID);

        try {
            spi_array_in = spi.write(spi_array_out);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Error writing to spi.", ex);
        }

        if (spi_array_in[4] != -91) {
            return "ERROR_SPI_RESPONSE";
        }

        for (int i = 0; i < 16; i++) {
            response.append(i * 2).append(String.format("%02X", spi_array_in[i + 4]));
        }

        return response.toString();
    }

    /**
     * Treats the return value from writing to the spi as a string. The string
     * begins at index 5 in the spi_array_in. If the spi_array_in[4] equals -91
     * then the information requested is invalid.
     *
     * @param msg_type
     * @return
     */
    private String spi_read_string(byte msg_type) {

        Arrays.fill(spi_array_out, (byte) 0);
        StringBuilder str = new StringBuilder();
        spiArrayBuilder(ADDRESS, msg_type);
        try {
            spi_array_in = spi.write(spi_array_out);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Error writing to spi.", ex);
        }

        // Check for invalid value in array
        if (spi_array_in[4] != -91) {
            return "ERROR_SPI_RESPONSE";
        }

        // Convert array into a string
        for (int i = 0; i < (LONGEST_SPI_TRANSFER - 5); i++) {
            str.append((char) spi_array_in[i + 5]);
        }
        return str.toString();
    }

    /**
     * Read from the board
     *
     * @param msg_type
     * @return
     */
    private int spi_read(byte msg_type) {

        Arrays.fill(spi_array_out, (byte) 0);
        spiArrayBuilder(ADDRESS, msg_type);
        try {
            spi_array_in = spi.write(spi_array_out);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Error writing to spi.", ex);
        }

//        if (spi_array_in[4] != -91) {
//            return ERROR_SPI_RESPONSE;
//        }
        LOG.log(Level.INFO, "8 spi_array_in[5]:{0}", (int) spi_array_in[5] & 0xff);
        return spi_array_in[5];
    }

    /**
     * Retrieve 2 bytes from the board
     *
     * @param msg_type
     * @return
     */
//    private int spi_read_16(byte msg_type) {
//        Arrays.fill(spi_array_out, (byte) 0);
//        spiArrayBuilder(ADDRESS, msg_type);
//
//        try {
//            spi_array_in = spi.write(spi_array_out);
//        } catch (IOException ex) {
//            LOG.log(Level.SEVERE, "Error writing to spi.", ex);
//        }
////        if (spi_array_in[4] != -91) {
////            return ERROR_SPI_RESPONSE;
////        }
//
//        LOG.log(Level.INFO, "16 spi_array_in[5]:{0}", (int) spi_array_in[5] & 0xff);
//        LOG.log(Level.INFO, "16 spi_array_in[6]:{0}", (int) spi_array_in[6] & 0xff);
//        return ((spi_array_in[5] & 0xff) << 8) | (spi_array_in[6] & 0xff);
//    }
//
    /**
     * Retrieve 4 bytes from the board
     *
     * @param msg_type
     * @return
     */
//    private int spi_read_32(byte msg_type) {
//        Arrays.fill(spi_array_out, (byte) 0);
//        spiArrayBuilder(ADDRESS, msg_type);
//
//        try {
//            spi_array_in = spi.write(spi_array_out);
//        } catch (IOException ex) {
//            LOG.log(Level.SEVERE, "Error writing to spi.", ex);
//        }
//
//        LOG.log(Level.INFO, "32 spi_array_in[4]:{0}", (int) spi_array_in[4] & 0xff);
//        LOG.log(Level.INFO, "32 spi_array_in[5]:{0}", (int) spi_array_in[5] & 0xff);
//        LOG.log(Level.INFO, "32 spi_array_in[6]:{0}", (int) spi_array_in[6] & 0xff);
//        LOG.log(Level.INFO, "32spi_array_in[7]:{0}", (int) spi_array_in[7] & 0xff);
//        LOG.log(Level.INFO, "32 spi_array_in[8]:{0}", (int) spi_array_in[8] & 0xff);
////        if (spi_array_in[4] != -91) {
////            return ERROR_SPI_RESPONSE;
////        }
//        return (((spi_array_in[5] & 0xff) << 24) | ((spi_array_in[6] & 0xff) << 16) | ((spi_array_in[7] & 0xff) << 8) | (spi_array_in[8] & 0xff));
//    }
    /**
     * Display the byte array as a String
     *
     * @param b
     * @return
     */
    private String displayString(byte[] byteArray) {
        String s = "";
        for (int x = 0; x < byteArray.length; ++x) {
            s += byteArray[x] + " ";
        }
        return s;
    }

}
